import numpy as np
import fetk.element
import fetk.material


def test_3d_tetraheron():
    coords = np.array([[0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1]], dtype=float)
    material = fetk.material.elastic({"E": 60.0, "nu": 0.25})
    element = fetk.element.C3D4()
    #ke = element.stiffness(coords, material)
    ve = element.volume(coords)
    assert np.allclose(ve, 1/6)
    expected = np.array(
        [
            [11, 5, -10, -2, -1, -3],
            [5, 11, 2, 10, -7, -21],
            [-10, 2, 44, -20, -34, 18],
            [-2, 10, -20, 44, 22, -54],
            [-1, -7, -34, 22, 35, -15],
            [-3, -21, 18, -54, -15, 75],
        ],
        dtype=float,
    )
    #assert np.allclose(ke, expected)
