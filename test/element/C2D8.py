import os
import json
import numpy as np
import fetk.element
import fetk.material
from fetk.fe_model import fe_model


def load_expected(datadir, file):
    return np.array(json.load(open(os.path.join(datadir, file))))


def test_C2D8_K(datadir):
    E, Nu, rho = 30e9, 0.2, 2400
    x = np.array(
        [[1, 0], [2, 0], [3, 0], [0.5, 2], [3.5, 2], [0, 4], [2, 4], [4, 4]],
        dtype=float,
    )
    connect = [0, 2, 7, 5, 1, 4, 6, 3]
    xc = x[connect]
    properties = {"thickness": 1.0}
    material = fetk.material.elastic({"E": E, "nu": Nu})
    element = fetk.element.C2D8E(**properties)
    ke = element.stiffness(xc, material)
    expected = load_expected(datadir, "C2D8_K.json")
    assert np.allclose(ke, expected, atol=1e-5, rtol=1e-5)


def test_C2D8_B(datadir):
    E, Nu, rho = 30e9, 0.2, 2400
    x = np.array(
        [[1, 0], [2, 0], [3, 0], [0.5, 2], [3.5, 2], [0, 4], [2, 4], [4, 4]],
        dtype=float,
    )
    connect = [0, 2, 7, 5, 1, 4, 6, 3]
    xc = x[connect]
    g = np.array([0, -9.81 * rho])
    properties = {"thickness": 1.0}
    element = fetk.element.C2D8E(**properties)
    expected = load_expected(datadir, "C2D8_B.json")
    fe = element.body_force(xc, g)
    assert np.allclose(fe, expected)


def test_C2D8_T(datadir):
    E, Nu, rho = 30e9, 0.2, 2400
    x = np.array(
        [[1, 0], [2, 0], [3, 0], [0.5, 2], [3.5, 2], [0, 4], [2, 4], [4, 4]],
        dtype=float,
    )
    connect = [0, 2, 7, 5, 1, 4, 6, 3]
    xc = x[connect]
    t = np.array([400e3, -300e3])
    properties = {"thickness": 1.0}
    element = fetk.element.C2D8E(**properties)
    expected = load_expected(datadir, "C2D8_F.json")
    fe = element.surface_force(xc, 0, t)
    assert np.allclose(fe, expected)


def test_C2D8_U(datadir):
    nodes = [[1, 1, 0], [2, 2, 0], [3, 3, 0], [4, 0.5, 2], [5, 3.5, 2], [6, 0, 4], [7, 2, 4], [8, 4, 4]]
    elements = [[1, 1, 3, 8, 6, 2, 5, 7, 4]]
    model = fe_model("Job-1", nodes, elements)

    E, Nu, rho = 30e9, 0.2, 2400
    properties = {"thickness": 1.0}

    element = fetk.element.C2D8E(**properties)
    material = fetk.material.elastic({"E": E, "nu": Nu}, density=rho)
    model.element_block("Block-1", elements=[1], element=element, material=material)

    model.nodeset("Nodeset-1", nodes=(6, 7, 8))
    model.boundary("Nodeset-1", "xy", 0)

    t = np.array([400e3, -300e3])
    sides = model.find_sides2d(lambda x: x[:, 1] == 0)
    model.sideset("Sideset-1", sides=sides)
    model.surface_force("Sideset-1", t)

    g = np.array([0, -9.81])
    model.element_set("Elemset-1", elements=(1,))
    model.gravity("Elemset-1", g)

    model.solve()

    # Abaqus solution
    ua = load_expected(datadir, "C2D8_U.json")
    assert np.allclose(model.solution.u, ua)
