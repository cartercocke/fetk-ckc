import pytest
import numpy as np
import fetk.material
import fetk.material.elastic as elastic


def test_material_factory():
    material = fetk.material.factory("elastic", {"E": 10.0, "nu": 0.3})
    assert isinstance(material, elastic)
    assert np.allclose(material.E, 10.0)
    assert np.allclose(material.nu, 0.3)


def test_material_error():
    with pytest.raises(ValueError):
        fetk.material.factory("fake", {"E": 10.0, "nu": 0.3})
