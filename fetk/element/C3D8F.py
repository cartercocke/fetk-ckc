import numpy as np

import fetk.dload
from .LND2 import L1D2
from .C2D4S import C2D4S
from .base import element, SOLID, OFF, ON
from fetk.util.geometry import face_normal


class C3D8F(element):
    """8-node fully integrated isoparametric hex element

    Notes
    -----
    - Node and element face numbering

                7 - - - - - - 6
              / .           / |
            /   .         /   |
          /     .       /     |
        4 - - - - - - 5       |
        |       .     |       |
        |       .     |       |
        |       3 . . | . . . 2
        |     .       |     /
        |   .         |   /
        | .           | /
        0 - - - - - - 1

    - Element is integrated by a 2x2x2 Gauss rule

    """

    signature = (SOLID, 3, ON, ON, ON, OFF, OFF, OFF, OFF, OFF, 1, 8)

    def init(self):
        self.edge = L1D2(A=1)
        self.face = C2D4S(thickness=1)

    def volume(self, xc):
        """Volume of the hex"""
        raise NotImplementedError

    def jacobian(self, xc, xg):
        """The Jacobian of the transformation from phyiscal to natural coordinates

        Returns
        -------
        J : float
            J = det(Jm), where Jm is the Jacobian matrix Jm = dx/ds

        """
        dNds = self.shapeder(xg)
        dxds = np.dot(dNds, xc)
        return np.linalg.det(dxds)

    def shape(self, xg):
        """Shape functions in the natural coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        s, t, u = xg
        N = np.array(
            [
                (1.0 - s) * (1.0 - t) * (1.0 - u),
                (1.0 + s) * (1.0 - t) * (1.0 - u),
                (1.0 + s) * (1.0 + t) * (1.0 - u),
                (1.0 - s) * (1.0 + t) * (1.0 - u),
                (1.0 - s) * (1.0 - t) * (1.0 + u),
                (1.0 + s) * (1.0 - t) * (1.0 + u),
                (1.0 + s) * (1.0 + t) * (1.0 + u),
                (1.0 - s) * (1.0 + t) * (1.0 + u),
            ]
        )
        return N / 8.0

    def shapeder(self, xg):
        """Derivatives of shape functions, wrt to natural coordinates

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        """
        s, t, u = xg
        dN = np.array(
            [
                [
                    -1.0 * (1.0 - t) * (1.0 - u),
                    1.0 * (1.0 - t) * (1.0 - u),
                    1.0 * (1.0 + t) * (1.0 - u),
                    -1.0 * (1.0 + t) * (1.0 - u),
                    -1.0 * (1.0 - t) * (1.0 + u),
                    1.0 * (1.0 - t) * (1.0 + u),
                    1.0 * (1.0 + t) * (1.0 + u),
                    -1.0 * (1.0 + t) * (1.0 + u),
                ],
                [
                    (1.0 - s) * -1.0 * (1.0 - u),
                    (1.0 + s) * -1.0 * (1.0 - u),
                    (1.0 + s) * 1.0 * (1.0 - u),
                    (1.0 - s) * 1.0 * (1.0 - u),
                    (1.0 - s) * -1.0 * (1.0 + u),
                    (1.0 + s) * -1.0 * (1.0 + u),
                    (1.0 + s) * 1.0 * (1.0 + u),
                    (1.0 - s) * 1.0 * (1.0 + u),
                ],
                [
                    (1.0 - s) * (1.0 - t) * -1.0,
                    (1.0 + s) * (1.0 - t) * -1.0,
                    (1.0 + s) * (1.0 + t) * -1.0,
                    (1.0 - s) * (1.0 + t) * -1.0,
                    (1.0 - s) * (1.0 - t) * 1.0,
                    (1.0 + s) * (1.0 - t) * 1.0,
                    (1.0 + s) * (1.0 + t) * 1.0,
                    (1.0 - s) * (1.0 + t) * 1.0,
                ],
            ]
        )
        return dN / 8.0

    def shapegrad(self, xc, xg):
        """Compute the derivatives of shape functions wrt to physical coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        dN : ndarray
            Derivatives of the shape function with respect to the physical
            coordinates

        Notes
        -----
        The shape functions are cast in terms of the natural coordinates.  Use
        the chain rule find their derivatives in the physical coordinates:

            dN/dx = dN/ds * ds/dx = dN/ds * inv(dx/ds)

        where

            dx/ds = d(sum(N * xi))/ds = sum(dN/ds * xi)

        """
        dNds = self.shapeder(xg)
        dxds = np.dot(dNds, xc)
        dsdx = np.linalg.inv(dxds)
        dNdx = np.dot(dsdx, dNds)
        return dNdx

    @property
    def gauss_points(self):
        """Gauss (integration) points

        Returns
        -------
        gp : ndarray
            s, t = gp[i] are s and t coordinates of the ith Gauss point, assuming
            counter clockwise orientation starting from the lower left corner

        """
        p = 1 / np.sqrt(3.0)
        return np.array(
            [
                [-p, -p, -p],
                [p, -p, -p],
                [p, p, -p],
                [-p, p, -p],
                [-p, -p, p],
                [p, -p, p],
                [p, p, p],
                [-p, p, p],
            ]
        )

    @property
    def gauss_weights(self):
        """Gauss (integration) weights

        Returns
        -------
        gw : ndarray
            w = gw[i] is the ith Gauss weight, assuming counter clockwise
            orientation starting from the lower left corner

        """
        return np.ones(8)

    @property
    def faces(self):
        return np.array(
            [
                [3, 2, 1, 0],
                [4, 5, 6, 7],
                [0, 1, 5, 4],
                [1, 2, 6, 5],
                [2, 3, 7, 6],
                [3, 0, 4, 7],
            ]
        )

    def bmatrix(self, xc, xg):
        """Compute the element B matrix which contains derivatives of shape
        functions, wrt to physical coordinates

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        B : ndarray
            The B matrix, or matrix containing derivatives of the shape function
            with respect to the physical coordinates

        Notes
        -----
        The shape functions are cast in terms of the natural coordinates.  Use
        the chain rule find their derivatives in the physical coordinates:

            dN/dx = dN/ds * ds/dx = dN/ds * inv(dx/ds)

        where

            dx/ds = d(sum(N * xi))/ds = sum(dN/ds * xi)

        """
        dNdx = self.shapegrad(xc, xg)
        num_node = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        B = np.zeros((6, num_dof_per_node * num_node))
        B[0, 0::num_dof_per_node] = dNdx[0, :]
        B[1, 1::num_dof_per_node] = dNdx[1, :]
        B[2, 2::num_dof_per_node] = dNdx[2, :]

        B[3, 0::num_dof_per_node] = dNdx[1, :]
        B[3, 1::num_dof_per_node] = dNdx[0, :]

        B[4, 1::num_dof_per_node] = dNdx[2, :]
        B[4, 2::num_dof_per_node] = dNdx[1, :]

        B[5, 0::num_dof_per_node] = dNdx[2, :]
        B[5, 2::num_dof_per_node] = dNdx[0, :]

        return B

    def stiffness(self, xc, material, *args):
        """Assemble the element stiffness

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates

        Returns
        -------
        ke : ndarray
            The element stiffess
            ke = integrate(B.T, C, B)

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        ke = np.zeros((num_nodes * num_dof_per_node, num_nodes * num_dof_per_node))
        for p in range(len(self.gauss_points)):
            xg = self.gauss_points[p]
            wg = self.gauss_weights[p]
            Be = self.bmatrix(xc, xg)
            Je = self.jacobian(xc, xg)
            Ce = material.stiffness(xc, ndir=3, nshr=3)
            ke += np.dot(np.dot(Be.T, Ce), Be) * Je * wg
        return ke

    def pmatrix(self, xg):
        """Compute the element P matrix

        Parameters
        ----------
        xg : ndarray
            si = xg[i] is the coordinate of the ith Gauss point

        Returns
        -------
        P : ndarray
            The S matrix, or matrix containing the shape functions.

        Notes
        -----
        The P matrix is setup such that values of an element constant field `f`
        can be interpolated to a point as:

            x = dot(P.T, f)

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        N = self.shape(xg)
        P = np.zeros((num_dof_per_node, num_nodes * num_dof_per_node))
        P[0, 0::num_dof_per_node] = N
        P[1, 1::num_dof_per_node] = N
        P[2, 2::num_dof_per_node] = N
        return P

    def force(self, xc, dload):
        """Compute the element force

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates

        Returns
        -------
        fe : ndarray
            The element force
            fe = integrate(transpose(P).f)

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        num_dof = num_nodes * num_dof_per_node
        fe = np.zeros(num_dof)
        if dload is None:
            return fe
        for (tag, face_no, f) in dload.items():
            if tag == fetk.dload.body_force:
                fe += self.body_force(xc, f[:num_dof_per_node])
            elif tag == fetk.dload.surface_force:
                fe += self.surface_force(xc, face_no, f[:2])
            elif tag == fetk.dload.pressure:
                face = self.faces[face_no]
                n = face_normal(xc[face])
                fe += self.surface_force(xc, face_no, f[0] * n)
        return fe

    def body_force(self, xc, f):
        """Calculate the nodal force contributions from a body load `f`

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=4) are the x coordinates, in the order shown above
            xc[:, 1] (len=4) are the y coordinates
        f : ndarray
            f[0] is the load on the body in the x direction
            f[1] is the load on the body in the y direction

        """
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        num_dof = num_nodes * num_dof_per_node
        fe = np.zeros(num_dof)
        for p in range(len(self.gauss_points)):
            xg = self.gauss_points[p]
            wg = self.gauss_weights[p]
            Je = self.jacobian(xc, xg)
            Pe = self.pmatrix(xg)
            # q = np.dot(Pe, f[:num_dof])
            fe += Je * wg * np.dot(Pe.T, f[:num_dof_per_node])
        return fe

    def surface_force(self, xc, face_no, q):
        """Calculate the nodal force contributions from a surface load `q`

        Parameters
        ----------
        xc : ndarray
            Nodal coordinates
            xc[:, 0] (len=4) are the x coordinates, in the order shown above
            xc[:, 1] (len=4) are the y coordinates
        face_no : ndarray
            The face number (using internal face numbering)
        q : ndarray
            The surface load

        """
        face = self.faces[face_no]
        num_nodes = self.num_nodes
        num_dof_per_node = self.num_dof_per_node
        fe = np.zeros(num_nodes * num_dof_per_node)
        xf = xc[face]
        for p in range(len(self.face.gauss_points)):
            wg = self.face.gauss_weights[p]
            xg = self.face.gauss_points[p]
            N = self.face.shape(xg)
            dNds = self.face.shapeder(xg)
            dxds = np.dot(dNds, xf)
            a = (dxds[0, 1] * dxds[1, 2]) - (dxds[1, 1] * dxds[0, 2])
            b = (dxds[0, 0] * dxds[1, 2]) - (dxds[1, 0] * dxds[0, 2])
            c = (dxds[0, 0] * dxds[1, 1]) - (dxds[1, 0] * dxds[0, 1])
            Jd = np.sqrt(a ** 2 + b ** 2 + c ** 2)
            for (i, ni) in enumerate(face):
                for j in range(num_dof_per_node):
                    I = ni * num_dof_per_node + j
                    fe[I] += wg * Jd * N[i] * q[j]
        return fe
