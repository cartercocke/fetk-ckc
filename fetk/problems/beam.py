import fetk.element
import fetk.material
from fetk.io import inpparse
from fetk.fe_model import fe_model


def run_file(file):
    inp = inpparse.parse_file(file)
    model = fe_model(inp.jobid, inp.nodes, inp.elements)
    for (i, material) in enumerate(inp.materials):
        mat = fetk.material.factory(material["model"], material["parameters"])
        inp.materials[i] = (material["id"], mat)
    for (id, block) in enumerate(inp.element_blocks):
        name = block.get("name", f"Block-{id + 1}")
        material_id = block["material"]
        for (id, material) in inp.materials:
            if id == material_id:
                break
        else:
            raise ValueError(f"Material for {name} not found")
        element_type = block["element type"]
        properties = block["element properties"]
        element = fetk.element.factory(element_type, **properties)
        model.element_block(
            name, elements=block["elements"], element=element, material=material
        )
    for (type, node_no, dof_label, magnitude) in inp.boundary:
        model.boundary(node_no, dof_label, magnitude, type=type)
    for (type, elem_no, side_no, values) in inp.dload:
        model.dload(type, elem_no, side_no, *values)
    model.solve()
    model.dump(format="exodus", T=model.solution.u, Q=model.solution.r)
    return model
