import numpy as np


def norm(v):
    return np.sqrt(np.dot(v, v))


def cost(a, b):
    return np.dot(a, b) / norm(a) / norm(b)


def edge_normal(xp):
    xp = np.asarray(xp)
    dx, dy = xp[1, :] - xp[0, :]
    v = np.array([dy, -dx], dtype=float)
    return v / np.sqrt(np.dot(v, v))


def face_normal(xp):
    raise NotImplementedError
