=======================
Internal representation
=======================

Internally, boundary conditions are represented by ``doftags`` and ``dofvals`` describing the conditions at each node.  ``doftags`` and ``dofvals`` should be constructed by ``fetk.boundary.fe_objects``.

-------------------
doftags and dofvals
-------------------

``doftags`` is a ``num_node`` by ``max_dof`` table containing an integer flag defining the type of boundary condition that is applied to each degree of freedom at each node.  The values are 0 for Neumann type constraint and 1 for Dirichlet.  ``dofvals`` is a ``num_node`` by ``max_dof`` table containing the corresponding magnitudes of the conditions prescribed in ``doftags``.

For the example in :ref:`fig_bc1`, ``doftags`` and ``dofvals`` are

.. code-block:: python

    doftags = np.array(
        [
            [0, 1, 0, 0, 0, 0, 0]
            [0, 0, 0, 0, 0, 0, 0]
            [0, 1, 0, 0, 0, 0, 0]
            [0, 0, 0, 0, 0, 0, 0]
            [0, 0, 0, 0, 0, 0, 0]
        ], dtype=int
    )
    dofvals = np.array(
        [
            [0, 0, 0, 0, 0, 0, 0]
            [0, 7200, 0, 0, 0, 0, 0]
            [0, 0, 0, 0, 0, 0, 0]
            [0, 0, 0, 0, 0, 0, 0]
            [0, 0, 0, 0, 0, 0, 0]
        ], dtype=float
    )

----------
Generating
----------

``doftags`` and ``dofvals`` are easily generated from the :ref:`nodes<defining_nodes>` and ``boundary`` tables:

.. code-block:: python

    import numpy as np
    from fetk.constants import max_dof

    dofvals = np.zeros((len(nodes), max_dof))
    doftags = np.zeros((len(nodes), max_dof), dtype=int)
    for condition in boundary:
        xn, type, dof, magnitude = condition
        n = node_map[xn]
        doftags[n, dof] = type
        dofvals[n, dof] = magnitude

``fetk.boundary.fe_objects`` implements the above algorithm with additional checks for correctness of input and can be used to generate ``doftags`` and ``doftags`` from user defined boundary data.
