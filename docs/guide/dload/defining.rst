.. _defining_dload:

==========================
Defining distributed laods
==========================

Distributed loads are defined in a table containing ``num_dload`` sublist items:

.. code-block:: python

   dload = [dload_0, dload_1, . . ., dload_n]

Each of the components of ``dload`` is a list defining the distributed load.

---------------------------
Definining individual loads
---------------------------

Individual distributed load conditions are defined as a list containing

.. code-block:: python

   dl = [ex, dof, magnitude]

where ``ex`` is the external element number,  1 for Dirichlet), ``dof`` is the coordinate direction of the condition (0 for :math:`x`, 1 for :math:`y`, and 2 for :math:`z`, 3 for temperature), and ``magnitude`` is the magnitude of the condition.

.. rubric:: Example

A beam oriented along the x direction having 5 elements has a constant distributed load :math:`q=-1000` applied along its top.

.. code-block:: python

   q = -1000
   dload = [
      [1, y, q],
      [2, y, q],
      [3, y, q],
      [4, y, q],
      [5, y, q],
   ]
