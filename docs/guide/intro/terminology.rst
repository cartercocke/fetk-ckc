============================
Organization and terminology
============================

--------
Coverage
--------

The report is organized as follows. Chapter 1 is an overview of design
principles, program organization, data classification, and nomenclature.
Chapters 2 through 7 deal with source data structures associated with nodes,
elements, degrees of freedom and loads. Chapters 8, 9 and 10 deal with auxiliary
data structures constructed in preparation for the computational phases.
Chapters 11, 12 and 13 deal with solution data structures. Chapter 14 offers
conclusions and recommendations.

----------
Data types
----------

A data structure is an object or collection of objects that store related
information, and is identified by a unique name.  Data structure identifiers are
case sensitive: ``force`` is not the same as ``Force``.

An object is any primitive or composite entity representable in computer memory
and optionally identified by a name. Objects may be primitive items, such as the
integer ``3``, a complicated composite item such as a complete PostScript graphic
file, a function definition, or a composition of simpler objects.

.....
Lists
.....

A list structure is a named sequence of objects. It is identified enclosing the
objects, separated by commas, with brackets. For example:

.. code-block:: python

   A = [a, b, c, d]

Here list ``A`` is a defined as the sequence of four objects named ``a``, ``b``,
``c``, and ``d``.

Lists may be embedded within lists through any depth. For example:

.. code-block:: python

   B = [a, b, [1, 2, 3, 4], d]

``B`` is a two-level list if ``a``, ``b``, ``[1, 2, 3, 4]``, and ``d``. In
practice lists of more than three levels are rarely needed for the present
study.

A list contained within a list is called a sublist.

............
Dictionaries
............

A dictionary is a compound data structure that consists of a list of ``key: value`` pairs. Each key in a key-value pair maps that key to its associative value.  Dictionaries are generally defined by a list of comma-separated key-value pairs wrapped in curly braces (``{}``). Each key is separated from its associated value by a colon (``:``):

.. code-block:: python

   A = {0: 1, "a": "b", "c": 2}

The keys should be immutable data types and the values can be any valid python object.

Once created, new entries can be added directly

.. code-block:: python

   A["foo"] = "spam"

.......
Tables
.......

Python does not have an intrinsic table data structure.  When the term table is
used in this document it usually refers to a list of lists.

........
Datasets
........

A dataset is a collection or grouping of data that pertains to a general or
specific activity. For example, "node definition dataset" means all the data
that pertains to the definition of nodal points. A dataset generally is a
collection of related lists, and does not necessarily need a name identifier.

......
Arrays
......

An array is a list of objects of the same type, each of which uses exactly the
same storage space.  Arrays are provided by the ``numpy`` (numerical python)
package. For example:

.. code-block:: python

   import numpy as np
   a = np.array([1, 4, 9, 16, 25])

is an integer array. This may be efficiently implemented as a primitive data
type in most program- ming languages. A two-dimensional array is an array of
one-dimensional arrays of identical length and type, and so on.

Arrays are often related to matrix objects. To emphasize the relationship
matrix/vector notation may be used.

------------------
Naming conventions
------------------

Three kind of names are associated with each of the major data structures
presented here:

Complete names
  For example, `Node Definition Table`. Such names are mnemonic but often
  too long for concise descriptions as well as programming.

Short names
  For a list structure this is an acronym normally formed with the initials of
  the complete name. For example, ``ndt`` for `Node Definition Table`. Letters
  are usually in lower case following the conventions of Table
  :ref:`tab_conventions`. For array structures that correspond directly to
  vector or matrix objects, the matrix or vector symbol, such as
  :math:`\mathbf{Kb}` or :math:`\mathbf{u}`, may serve as a short name.

Program names
  These are used in the computer implementation. They are always shown in
  ``typewriter`` font. In this document, program names are usually taken to be the
  same as short names, but this is a matter of convenience. Programmers are of
  course free to make their own choices; see Note below.

.. note::

  In ``fem`` we follow the naming conventions described in `pep8
  <https://www.python.org/dev/peps/pep-0008/>`_ whereby variables and functions
  are named using ``snake_case``, classes with ``CamelCase``, and constants with
  ``ALL_CAPS``.

...................................
Conventions for short name acronyms
...................................

.. _tab_conventions:

.. table:: Conventions for short name acronyms

   +--------+----------------------------------------------------------------------+
   | Letter | Meaning                                                              |
   +========+======================================================================+
   | A      | Activity, Address, Arrangement, Assembly a Acceleration (vector)     |
   +--------+----------------------------------------------------------------------+
   | B      | Bandwidth, Boundary, Built-in                                        |
   +--------+----------------------------------------------------------------------+
   | b      | Bodyforce (vector)                                                   |
   +--------+----------------------------------------------------------------------+
   | C      | Configuration, Connection, Constitutive, Constraint c Constitutive   |
   |        | (individual)                                                         |
   +--------+----------------------------------------------------------------------+
   | D      | Definition                                                           |
   +--------+----------------------------------------------------------------------+
   | d      | Deformation (vector)                                                 |
   +--------+----------------------------------------------------------------------+
   | E      | Element                                                              |
   +--------+----------------------------------------------------------------------+
   | e      | Element (individual)                                                 |
   +--------+----------------------------------------------------------------------+
   | F      | Fabrication, Freedom, Flexibility, Fluid                             |
   +--------+----------------------------------------------------------------------+
   | f      | Freedom (individual), Fabrication (individual) G Global, Generalized |
   +--------+----------------------------------------------------------------------+
   | g      | Gradient (vector)                                                    |
   +--------+----------------------------------------------------------------------+
   | H      | Heat, History                                                        |
   +--------+----------------------------------------------------------------------+
   | I      | Identity, Initial, Individual, Interface                             |
   +--------+----------------------------------------------------------------------+
   | i      | Internal                                                             |
   +--------+----------------------------------------------------------------------+
   | J      | Energy                                                               |
   +--------+----------------------------------------------------------------------+
   | j      | Jump                                                                 |
   +--------+----------------------------------------------------------------------+
   | K      | Stiffness                                                            |
   +--------+----------------------------------------------------------------------+
   | k      | Conductivity                                                         |
   +--------+----------------------------------------------------------------------+
   | L      | Local, Load                                                          |
   +--------+----------------------------------------------------------------------+
   | M      | Mass, Master, Matrix, Multiplier                                     |
   +--------+----------------------------------------------------------------------+
   | m      | Moment (vector), Multiplier (individual)                             |
   +--------+----------------------------------------------------------------------+
   | N      | Node, Nonlinear                                                      |
   +--------+----------------------------------------------------------------------+
   | n      | Node (individual)                                                    |
   +--------+----------------------------------------------------------------------+
   | O      | Object                                                               |
   +--------+----------------------------------------------------------------------+
   | P      | Partition, Potential, Property                                       |
   +--------+----------------------------------------------------------------------+
   | p      | Partition (individual), Momentum (vector), pointer                   |
   +--------+----------------------------------------------------------------------+
   | Q      | Force as freedom conjugate⇤                                          |
   +--------+----------------------------------------------------------------------+
   | q      | Force (vector)                                                       |
   +--------+----------------------------------------------------------------------+
   | R      | Response, Rigid                                                      |
   +--------+----------------------------------------------------------------------+
   | r      | Rotation (vector)                                                    |
   +--------+----------------------------------------------------------------------+
   | S      | Shock, Skyline, Spectrum, State, Structure, Subdomain                |
   +--------+----------------------------------------------------------------------+
   | s      | Subdomain (individual)                                               |
   +--------+----------------------------------------------------------------------+
   | T      | Table, Tag, Tangent, Temperature, Time                               |
   +--------+----------------------------------------------------------------------+
   | t      | Translation (vector)                                                 |
   +--------+----------------------------------------------------------------------+
   | U      | Unconstrained, Unified                                               |
   +--------+----------------------------------------------------------------------+
   | u      | Displacement (vector)                                                |
   +--------+----------------------------------------------------------------------+
   | V      | Valency, Vector, Vibration, Visualization                            |
   +--------+----------------------------------------------------------------------+
   | v      | Velocity (vector)                                                    |
   +--------+----------------------------------------------------------------------+
   | W      | Weight, Width                                                        |
   +--------+----------------------------------------------------------------------+
   | w      | Frequencies                                                          |
   +--------+----------------------------------------------------------------------+
   | X      | Eigenvectors                                                         |
   +--------+----------------------------------------------------------------------+
   | x      | External                                                             |
   +--------+----------------------------------------------------------------------+
   | Y      | Strain                                                               |
   +--------+----------------------------------------------------------------------+
   | Z      | Stress                                                               |
   +--------+----------------------------------------------------------------------+

------------------------
Implementation languages
------------------------

The ease of implementation of flexible data structures depends on the
implementation language. Generally there is a tradeoff between computational
efficiency and human effort, and it is important to balance the two
requirements. Two general requirements are:

* Many computational data structures can be implemented as arrays, but the size
  is only known at run time. Thus, any conventional programming language that
  supports dynamic storage management may be used.

* Source data structures may be best implemented as lists for maximum
  flexibility and to facilitate program evolution, because for the front end
  computational efficiency is not usually a major issue.

Following is a brief review of various programming languages for implementing
FEM applications.

C, C++, Fortran 90
  These languages directly support arrays as primitive objects as well as
  dynamic storage allocation. Lists are not directly supported as primitives and
  must be implemented as programmer-defined data types: structures in C, classes
  in C++, derived types in Fortran 90.

Matlab, Fortran 77
  These languages do not support lists or the creation of derived data types,
  although Matlab handles dynamic storage allocation. Fortran 77 may be
  considered in kernel implementations if called from master C routines that
  handle dynamic resource allocation. This has the advantage of reuse of the
  large base of existing FE code. However, mixed language programming can run
  into serious transportability problems.

Python
  Because this language supports primitive and dynamic list operations at run
  time, the implementation of all data structures described here is
  straightforward. This simplicity is paid by run time penalties of order
  100-10000. Hence Python deserves consideration as an instructional and
  rapid prototyping tool, but should not be viewed as a production vehicle. It
  is in this spirit that the presently implementation is offered.
